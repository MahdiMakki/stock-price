from setuptools import setup, find_packages

setup(
      include_package_data=True,

      name='stock-price',
      version='0.1',
      description='APPL Historic Daily Stock Price ',
      url='https://bitbucket.org/MahdiMakki/stock-price/',
      author='Mahdi Makki',
      author_email='Mahdi.Makki@ey.com',
      license='EY',
      packages=find_packages(),  #Finds packages within directory
                                 # can pass exclude=['filesToExclude']
                                 #to exclude packaging of certain package

      install_requires=['pandas', 'matplotlib'
                        , 'boto3']  #What additional
                                    #  libraries have to be
                                    # installed when installing this package
    )