import pandas as pd
import matplotlib.pyplot as plt
import boto3 as bt

def printAaplData():

    data = pd.read_csv("AAPL.csv")
    print(data.head())
    data.plot()
    plt.show()


printAaplData()
